## Agenda

- Answer questions (Async Reading)
- Address items in Plan questions issue

### Week 3 Specifics
- Track change through the value stream at the group and project level together
- Verify web app looks as expected in review app

~meeting_agenda